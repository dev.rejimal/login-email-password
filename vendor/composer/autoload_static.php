<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd97bcdf1bea142706e11a2a53e109bd5
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\LoginEmailPassword\\' => 27,
        ),
        'J' => 
        array (
            'Joshbrw\\LaravelModuleInstaller\\' => 31,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\LoginEmailPassword\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
        'Joshbrw\\LaravelModuleInstaller\\' => 
        array (
            0 => __DIR__ . '/..' . '/joshbrw/laravel-module-installer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd97bcdf1bea142706e11a2a53e109bd5::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd97bcdf1bea142706e11a2a53e109bd5::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
